rom django import forms
from tasks.models import Task


class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = [
            "name",
            "start_"
            "description",
            "owner",
        ]
