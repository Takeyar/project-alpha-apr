from django import forms
from tasks.models import Task


class Form(forms.ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start date",
            "due date",
            "project",
            "assignee",
        ]
