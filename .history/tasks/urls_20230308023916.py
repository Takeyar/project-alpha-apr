from django.contrib import admin
from django.urls import path
from tasks.views import create_task


urlpatterns = [
    ("create/", create_task, name="create_task"),
]
