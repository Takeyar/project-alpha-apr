from django.contrib import admin
from django.urls import path
from tasks.views import create_task


urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", show_my_tasks, naem)
]
