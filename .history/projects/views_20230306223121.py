from django.shortcuts import render,redi
from projects.models import Project


def list_projects(request):
    project = Project.objects.all()
    context = {
        "projects": project,
    }
    return render(request, "projects/list.html", context)
