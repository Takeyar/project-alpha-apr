from django.contrib import admin
from django.urls import path
from projects.views import list


urlpatterns = [
    path("", list_projects, name="list_projects")
]
