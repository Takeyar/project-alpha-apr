from django.shortcuts import render
from projects.models import Project


def Project_list(request):
    project = Project.objects.all()
    context = {
        "projects": project,
    }
    return render(request, projects )
