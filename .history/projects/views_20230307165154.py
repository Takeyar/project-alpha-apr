from django.shortcuts import render
from projects.models import Project
from .models import Project
from django.contrib.auth.decorators import login_required


def list_projects(request):
    project = Project.objects.filter(owner)
    context = {
        "projects": project,
    }
    return render(request, "projects/list.html", context)
